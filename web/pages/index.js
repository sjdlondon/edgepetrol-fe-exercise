import { useQuery, gql } from '@apollo/client'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link'
import tableStyles from '../styles/Table.module.css'
import homeStyles from '../styles/Home.module.css'
import styles from "../styles/Home.module.css";

const STATIONS_QUERY = gql`
  {
    stations {
      id
      name
      metrics {
        volume
        margin
        profit
      }
    }
  }
`

export default function Home() {
  const { data, loading, error } = useQuery(STATIONS_QUERY)

  if (loading) return (
    <div className={homeStyles.container}>
      <div className={homeStyles.main}>
        { loading }
      </div>
    </div>
  )
  if (error) return (
    <div className={homeStyles.container}>
      <div className={homeStyles.main}>
        { error.message }
      </div>
    </div>
  )

  return (
    <div className={homeStyles.container}>
      <main className={homeStyles.main}>
        <h1>Stations</h1>
        <table className={tableStyles.table}>
          <thead>
            <tr>
              <th>#</th>
              <th>Station Name</th>
              <th>Metrics (Volume)</th>
              <th>Metrics (Margin)</th>
              <th>Metrics (Profit)</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              data && (
                data.stations.map((station) => (
                  <tr key={station.id}>
                    <td>{station.id}</td>
                    <td>{station.name}</td>
                    <td>{station.metrics.volume}</td>
                    <td>{station.metrics.margin}</td>
                    <td>{station.metrics.profit}</td>
                    <td>
                      <Link href={{
                        pathname: '/station/[slug]',
                        query: { slug: station.id }
                      }}>
                        <FontAwesomeIcon icon={faEye}/>
                      </Link>
                    </td>
                  </tr>
                ))
              )
            }
          </tbody>
        </table>
      </main>
    </div>
  )
}
