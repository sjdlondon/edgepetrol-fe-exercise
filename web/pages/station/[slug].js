import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link'
import { useQuery, gql } from '@apollo/client'
import styles from '../../styles/Home.module.css'

const STATION_QUERY = gql`
  query($id: Float!){
    station(id: $id) {
      id
      name
      metrics {
        volume
        margin
        profit
      }
    }
  }
`

export const Station = () => {
  const router = useRouter()
  const { data, loading, error } = useQuery(STATION_QUERY, {
    variables: { id: parseFloat(router.query.slug) }
  })

  if (loading) return (
    <div className={styles.container}>
      <div className={styles.main}>
        { loading }
      </div>
    </div>
  )
  if (error) return (
    <div className={styles.container}>
      <div className={styles.main}>
        { error.message }
      </div>
    </div>
  )

  return (
    <>
      <div className={styles.container}>
        <main className={styles.main}>
          <a href="/">
            <FontAwesomeIcon icon={faChevronLeft} />&nbsp;Back to List
          </a>
          {
            data && (
              <>
                <h1>#{data.station.id}&nbsp;{data.station.name}</h1>
                <p>
                  Metrics in Volume: {data.station.metrics.volume}
                  <br />
                  Metrics in Margin: {data.station.metrics.margin}
                  <br />
                  Metrics in Profit: {data.station.metrics.profit}
                </p>
              </>
            )
          }
        </main>
      </div>
    </>
  )
}

export default Station
